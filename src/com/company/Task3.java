package com.company;

import java.util.Scanner;

public class Task3 {


    //TASK 3
    public static void main(String[] args)  {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите число:");

        if (scr.hasNextInt()) {
            int n = scr.nextInt();
            int result = 0;
            for (int x = 0; x < n+1; x++) {
                result += x;
            }
            System.out.println("Спасибо! Вы ввели число " + result);

        } else {
            System.out.println("Извините, но это явно не число. Перезапустите программу и попробуйте снова!");
        }


    }
}
